﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using SpamDetectorFYPML.ConsoleApp;
using SpamDetectorFYPML.Model;
using System.Net.Mail;

namespace SpamDetectorFYPMLUnitTests
{
    [TestClass]
    public class SpamAnalysisTests
    {
        [TestMethod]
        public void CreateEmailModel_ReturnsCorrectModelInput()
        {
            string testPath = "testPath";
            string testEmail = "Subject: TestSubject \n TestEmail";

            Mock<SpamAnalysis> testSpamAnalysis = new Mock<SpamAnalysis>();
            testSpamAnalysis.CallBase = true;
            testSpamAnalysis.Setup(x => x.ReadEmail(testPath)).Returns(testEmail);

            var expectedModelInput = new ModelInput
            {
                EmailSubject = "TestSubject",
                EmailBody = "TestEmail",
            };

            var actualModelInput = testSpamAnalysis.Object.CreateEmailModel(testPath);

            Assert.AreEqual(expectedModelInput.EmailSubject, actualModelInput.EmailSubject);
            Assert.AreEqual(expectedModelInput.EmailBody, actualModelInput.EmailBody);
        }
    }
}
