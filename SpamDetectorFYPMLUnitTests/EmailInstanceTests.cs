﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using SpamDetectorFYPML.ConsoleApp;
using System.Net.Mail;

namespace SpamDetectorFYPMLUnitTests
{
    [TestClass]
    public class EmailInstanceTests
    {
        [TestMethod]
        public void ComposeEmail_ReturnsCorrectMailMessage()
        {
            string testPath = "testPath";
            string testEmail = "Subject: TestSubject \n TestEmail";

            Mock<EmailInstance> testEmailInstance = new Mock<EmailInstance>();
            testEmailInstance.CallBase = true;
            testEmailInstance.Setup(x => x.ReadFile(testPath)).Returns(testEmail);

            var expectedMailMessage = new MailMessage
            {
                Subject = "TestSubject",
                Body = "TestEmail",
            };

            var actualMailMessage = testEmailInstance.Object.ComposeEmail(testPath);

            Assert.AreEqual(expectedMailMessage.Subject, actualMailMessage.Subject);
            Assert.AreEqual(expectedMailMessage.Body, actualMailMessage.Body);
        }
    }
}
