﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using SpamDetectorFYPML.ConsoleApp;
using SpamDetectorFYPML.Model;
using System.Net.Mail;

namespace SpamDetectorFYPMLUnitTests
{
    [TestClass]
    public class ModelBuilderTests
    {
        [TestMethod]
        public void CompareModelWithBestModel_ReturnsTrue()
        {
            double testBestModelAccuracy = 25;
            ModelEvaluation modelEvaluation = new ModelEvaluation
            {
                AverageMicroAccuracy = 50,
            };

            Assert.IsFalse(ModelBuilder.CompareModelWithBestModel(modelEvaluation, testBestModelAccuracy));
        }

        [TestMethod]
        public void CompareModelWithBestModel_ReturnsFalse()
        {
            double testBestModelAccuracy = 75;
            ModelEvaluation modelEvaluation = new ModelEvaluation
            {
                AverageMicroAccuracy = 50,
            };

            Assert.IsTrue(ModelBuilder.CompareModelWithBestModel(modelEvaluation, testBestModelAccuracy));
        }
    }
}
