﻿using Microsoft.ML;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using SpamDetectorFYPML.ConsoleApp;
using System.Net.Mail;

namespace SpamDetectorFYPMLUnitTests
{
    [TestClass]
    public class ModelEvaluationTests
    {
        [TestMethod]
        public void ModelEvaluation_ReturnsCorrectMicroAccuracy()
        {
            ModelEvaluation modelEvaluation = new ModelEvaluation
            {
                AverageMicroAccuracy = 50,
            };

            Assert.AreEqual(modelEvaluation.AverageMicroAccuracy, 50);
        }

        [TestMethod]
        public void ModelEvaluation_ReturnsCorrectMacroAccuracy()
        {
            ModelEvaluation modelEvaluation = new ModelEvaluation
            {
                AverageMacroAccuracy = 50,
            };

            Assert.AreEqual(modelEvaluation.AverageMacroAccuracy, 50);
        }

        [TestMethod]
        public void ModelEvaluation_ReturnsCorrectModelData()
        {
            ITransformer test = null;
            ModelEvaluation modelEvaluation = new ModelEvaluation
            {
                ModelData = test,
            };

            Assert.AreEqual(modelEvaluation.ModelData, test);
        }

        [TestMethod]
        public void ModelEvaluation_ReturnsCorrectModelSavePath()
        {
            var testSavePath = "testSavePath";
            ModelEvaluation modelEvaluation = new ModelEvaluation
            {
                ModelSavePath = testSavePath,
            };

            Assert.AreEqual(modelEvaluation.ModelSavePath, testSavePath);
        }
    }
}
