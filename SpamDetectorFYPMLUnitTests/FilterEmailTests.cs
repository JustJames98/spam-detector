using Microsoft.VisualStudio.TestTools.UnitTesting;
using SpamDetectorFYPML.ConsoleApp;

namespace SpamDetectorFYPMLUnitTests
{
    [TestClass]
    public class FilterEmailTests
    {
        [TestMethod]
        public void RemoveTabsAndMultiSpaceTest_RemovesTabFromString()
        {
            string testTab = "123   456";
            string expectedTab = "123 456";
            var actualTab = FilterEmail.RemoveTabsAndMultiSpace(testTab);

            Assert.AreEqual(expectedTab, actualTab);
        }

        [TestMethod]
        public void RemoveTabsAndMultiSpaceTest_RemovesMultispaceFromString()
        {
            string testMultispace = "123   456";
            string expectedMultispace = "123 456";
            var actualMultispace = FilterEmail.RemoveTabsAndMultiSpace(testMultispace);

            Assert.AreEqual(expectedMultispace, actualMultispace);
        }

        [TestMethod]
        public void FilterEmailSubject_ReturnsCorrectString()
        {
            string testEmail = "Subject: TestSubject \n TestEmail";

            string expectedSubject = "TestSubject";

            var actualSubject = FilterEmail.FilterEmailSubject(testEmail);

            Assert.AreEqual(expectedSubject, actualSubject);
        }

        [TestMethod]
        public void FilterEmailBody_ReturnsCorrectString()
        {
            string testEmail = "Subject: TestSubject \n TestEmail";

            string expectedSubject = "TestEmail";

            var actualSubject = FilterEmail.FilterEmailBody(testEmail);

            Assert.AreEqual(expectedSubject, actualSubject);
        }
    }
}
