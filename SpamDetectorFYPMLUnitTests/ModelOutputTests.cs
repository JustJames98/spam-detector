﻿using Microsoft.ML;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using SpamDetectorFYPML.ConsoleApp;
using SpamDetectorFYPML.Model;
using System.Net.Mail;

namespace SpamDetectorFYPMLUnitTests
{
    [TestClass]
    public class ModelOutputTests
    {
        [TestMethod]
        public void ModelOutput_ReturnsCorrectPrediction()
        {
            string testPrediction = "TestPrediction";
            ModelOutput modelOutput = new ModelOutput
            {
                Prediction = testPrediction,
            };

            Assert.AreEqual(modelOutput.Prediction, testPrediction);
        }

        [TestMethod]
        public void ModelOutput_ReturnsCorrectScore()
        {
            float[] testScore = new float[2];
            ModelOutput modelOutput = new ModelOutput
            {
                Score = testScore,
            };

            Assert.AreEqual(modelOutput.Score, testScore);
        }
    }
}
