﻿using Microsoft.ML;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using SpamDetectorFYPML.ConsoleApp;
using SpamDetectorFYPML.Model;
using System.Net.Mail;

namespace SpamDetectorFYPMLUnitTests
{
    [TestClass]
    public class ModelInputTests
    {
        [TestMethod]
        public void ModelInput_ReturnsCorrectSpam()
        {
            string testSpam = "TestSpam";
            ModelInput modelInput = new ModelInput
            {
                Spam = testSpam,
            };

            Assert.AreEqual(modelInput.Spam, testSpam);
        }

        [TestMethod]
        public void ModelInput_ReturnsCorrectEmailSubject()
        {
            string testSubject = "TestSubject";
            ModelInput modelInput = new ModelInput
            {
                EmailSubject = testSubject,
            };

            Assert.AreEqual(modelInput.EmailSubject, testSubject);
        }

        [TestMethod]
        public void ModelInput_ReturnsCorrectEmailBody()
        {
            string testBody = "TestBody";
            ModelInput modelInput = new ModelInput
            {
                EmailBody = testBody,
            };

            Assert.AreEqual(modelInput.EmailBody, testBody);
        }
    }
}
