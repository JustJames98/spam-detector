using Microsoft.ML.Data;

namespace SpamDetectorFYPML.Model
{
    public class ModelInput
    {
        [ColumnName("Spam"), LoadColumn(0)]
        public string Spam { get; set; }

        [ColumnName("EmailSubject"), LoadColumn(1)]
        public string EmailSubject { get; set; }

        [ColumnName("EmailBody"), LoadColumn(2)]
        public string EmailBody { get; set; }
    }
}
