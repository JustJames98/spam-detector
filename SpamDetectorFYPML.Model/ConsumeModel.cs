using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.ML;
using SpamDetectorFYPML.Model;

namespace SpamDetectorFYPML.Model
{
    public class ConsumeModel
    {
        private static Lazy<PredictionEngine<ModelInput, ModelOutput>> PredictionEngine = new Lazy<PredictionEngine<ModelInput, ModelOutput>>(CreatePredictionEngine);

        // Method for consuming model in your app
        public static ModelOutput Predict(ModelInput input)
        {
            ModelOutput result = PredictionEngine.Value.Predict(input);
            return result;
        }

        public static PredictionEngine<ModelInput, ModelOutput> CreatePredictionEngine()
        {
            // Create new MLContext
            MLContext mlContext = new MLContext();
            // Load model & create prediction engine
            string modelPath = ChooseModel();
            ITransformer mlModel = mlContext.Model.Load(modelPath, out var modelInputSchema);
            var predEngine = mlContext.Model.CreatePredictionEngine<ModelInput, ModelOutput>(mlModel);

            return predEngine;
        }

        public static string ChooseModel()
        {
            Console.WriteLine("Enter 1 for LightGbm");
            Console.WriteLine("Enter 2 for LinearSvm");
            Console.WriteLine("Enter 3 for SgdCalibrated");
            Console.WriteLine("Enter 4 for SgdNonCalibrated");
            Console.WriteLine("Enter 5 for LdSvm");
            Console.WriteLine("Enter 6 for FastForest");
            Console.WriteLine("Enter 7 for FastTree");

            var input = Console.ReadLine();
            var test = "9";
            switch (input)
            {
                case "1":
                    Console.WriteLine("Using LightGbm Model");
                    return @"C:\Users\james\AppData\Local\Temp\MLVSTools\SpamDetectorFYPML\SpamDetectorFYPML.Model\MLModelLightGbm" + test + ".zip";
                case "2":
                    Console.WriteLine("Using LinearSvm Model");
                    return @"C:\Users\james\AppData\Local\Temp\MLVSTools\SpamDetectorFYPML\SpamDetectorFYPML.Model\MLModelLinearSvm" + test + ".zip";
                case "3":
                    Console.WriteLine("Using SgdCalibrated Model");
                    return @"C:\Users\james\AppData\Local\Temp\MLVSTools\SpamDetectorFYPML\SpamDetectorFYPML.Model\MLModelSgdCalibrated" + test + ".zip";
                case "4":
                    Console.WriteLine("Using SgdNonCalibrated Model");
                    return @"C:\Users\james\AppData\Local\Temp\MLVSTools\SpamDetectorFYPML\SpamDetectorFYPML.Model\MLModelSgdNonCalibrated" + test + ".zip";
                case "5":
                    Console.WriteLine("Using LdSvm Model");
                    return @"C:\Users\james\AppData\Local\Temp\MLVSTools\SpamDetectorFYPML\SpamDetectorFYPML.Model\MLModelLdSvm" + test + ".zip";
                case "6":
                    Console.WriteLine("Using FastForest Model");
                    return @"C:\Users\james\AppData\Local\Temp\MLVSTools\SpamDetectorFYPML\SpamDetectorFYPML.Model\MLModelFastForest" + test + ".zip";
                case "7":
                    Console.WriteLine("Using FastTree Model");
                    return @"C:\Users\james\AppData\Local\Temp\MLVSTools\SpamDetectorFYPML\SpamDetectorFYPML.Model\MLModelFastTree" + test + ".zip";
                default:
                    Console.WriteLine("Not a valid model number, Defaulting to LightGbm Model");
                    return @"C:\Users\james\AppData\Local\Temp\MLVSTools\SpamDetectorFYPML\SpamDetectorFYPML.Model\MLModelLightGbm" + test + ".zip";
            }
        }
    }
}
