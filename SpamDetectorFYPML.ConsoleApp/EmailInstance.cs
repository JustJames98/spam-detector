﻿using System;
using System.IO;
using System.Net.Mail;
using System.Text.RegularExpressions;
using System.Threading;

namespace SpamDetectorFYPML.ConsoleApp
{
    public class EmailInstance
    {
        public readonly MailAddress to = new MailAddress("james@localhost.com");
        public readonly MailAddress from = new MailAddress("james@localhost.com");

        public MailMessage ComposeEmail(string path)
        {
            string emailBody = this.ReadFile(path);
            MailMessage message = new MailMessage(this.from, this.to);
            try
            {
                message.Subject = FilterEmail.FilterEmailSubject(emailBody);
                message.Body = FilterEmail.FilterEmailBody(emailBody);
            }
            catch (ArgumentException e)
            {
                message.Subject = "Error";
                message.Body = e.Message;
            }

            return message;
        }

        public void SendEmail(object message)
        {
            using (var client = new SmtpClient("localhost", 25))
            {
                client.Send((MailMessage)message);
            }
        }

        public virtual string ReadFile(string path)
        {
            string result = "";
            using (var sr = new StreamReader(path))
            {
                result = sr.ReadToEnd();
            }
            return result;
        }
    }
}

