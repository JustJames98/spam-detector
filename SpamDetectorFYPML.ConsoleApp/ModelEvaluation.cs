﻿using Microsoft.ML;
using System;
using System.Collections.Generic;
using System.Text;

namespace SpamDetectorFYPML.ConsoleApp
{
    public class ModelEvaluation
    {
        public double AverageMicroAccuracy { get; set; }

        public double AverageMacroAccuracy { get; set; }

        public ITransformer ModelData { get; set; }

        public string ModelSavePath { get; set; }
    }
}
