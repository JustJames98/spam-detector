using System;

namespace SpamDetectorFYPML.ConsoleApp
{
    static class Program
    {
        static void Main(string[] args)
        {
            var keepRunning = true;
            while (keepRunning)
            {
                Console.WriteLine("Enter 1 for SpamAnalysis");
                Console.WriteLine("Enter 2 for CSVCreation");
                Console.WriteLine("Enter 3 for EmailSender");
                Console.WriteLine("Enter 4 for ModelBuilder");
                var input = Console.ReadLine();

                switch (input)
                {
                    case "1":
                        Console.WriteLine("Running Spam Analysis");
                        SpamAnalysis.RunSpamAnalysis();
                        keepRunning = false;
                        break;
                    case "2":
                        Console.WriteLine("Running CSVCreation");
                        CSVCreator.ProcessEmails();
                        break;
                    case "3":
                        Console.WriteLine("Running EmailSender");
                        EmailSender.RunEmailSender();
                        break;
                    case "4":
                        Console.WriteLine("Running ModelBuilder");
                        ModelBuilder.CreateModel();
                        break;
                    default:
                        Console.WriteLine("Not a valid choice");
                        break;
                }
            }
        }


    }
}
