﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace SpamDetectorFYPML.ConsoleApp
{
    public static class CSVCreator
    {
        private const string savePath = "C:\\Users\\james\\Desktop\\Third year uni\\CS3607 Final Year Project\\CSVEmailsEnronSplit.csv";
        private const string readPathSpam = "C:\\Users\\james\\Desktop\\Third year uni\\CS3607 Final Year Project\\EnronTrainingFiles\\spam";
        private const string readPathHam = "C:\\Users\\james\\Desktop\\Third year uni\\CS3607 Final Year Project\\EnronTrainingFiles\\ham";
        private static int i = 0;
        private static int j = 0;

        public static void ProcessEmails()
        {
            StringBuilder CSVString = new StringBuilder();
            CSVString.Append("Spam,EmailSubject,EmailBody");
            CSVString.Append("\n");

            var spamEmails = Directory.EnumerateFiles(readPathSpam, "*.txt");
            var hamEmails = Directory.EnumerateFiles(readPathHam, "*.txt");

            foreach (var file in spamEmails)
            {
                var result = ReadEmail(file);
                CSVString.Append("TRUE" + ",");
                CSVString.Append(FilterEmail.FilterEmailSubject(result) + ",");
                CSVString.Append(FilterEmail.FilterEmailBody(result));
                CSVString.Append("\n");

                i++;
                Console.WriteLine("Spam Files Processed: " + i);
            }

            foreach (var file in hamEmails)
            {
                var result = ReadEmail(file);
                CSVString.Append("FALSE" + ",");
                CSVString.Append(FilterEmail.FilterEmailSubject(result) + ",");
                CSVString.Append(FilterEmail.FilterEmailBody(result));
                CSVString.Append("\n");

                j++;
                Console.WriteLine("Ham Files Processed: " + j);
            }

            SaveCSV(savePath, CSVString.ToString());
        }

        public static string ReadEmail(string readPath)
        {
            string result = "";
            try
            {
                using (var sr = new StreamReader(readPath))
                {
                    result = sr.ReadToEnd();
                }
            }
            catch (FileNotFoundException)
            {
                result = "Deleted due to presence of Malicious Objects";
            }
            return result.Replace(",", "");
        }

        public static void SaveCSV(string savePath, string CSVString)
        {
            File.WriteAllText(savePath, CSVString);
        }
    }
}