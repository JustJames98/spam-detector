﻿using System;
using System.IO;
using System.Net.Mail;
using System.Threading;

namespace SpamDetectorFYPML.ConsoleApp
{
    static class EmailSender
    {
        private const string readPathSpam = "C:\\Users\\james\\Desktop\\Third year uni\\CS3607 Final Year Project\\EnronTestingFiles\\spam";
        private const string readPathHam = "C:\\Users\\james\\Desktop\\Third year uni\\CS3607 Final Year Project\\EnronTestingFiles\\ham";
        private const int numberEmailSend = 5; //75420 max
        public static void RunEmailSender()
        {
            var emailInstance = new EmailInstance();
            var spamEmails = Directory.EnumerateFiles(readPathSpam, "*.txt");
            var hamEmails = Directory.EnumerateFiles(readPathHam, "*.txt");
            var i = 0;
            foreach (var file in spamEmails)
            {
                var email = emailInstance.ComposeEmail(file);
                bool sent = ThreadPool.QueueUserWorkItem(new WaitCallback(emailInstance.SendEmail), email);
                i++;
                Console.WriteLine((i + 1) + " emails sent.");
            }

            foreach (var file in hamEmails)
            {
                var email = emailInstance.ComposeEmail(file);
                bool sent = ThreadPool.QueueUserWorkItem(new WaitCallback(emailInstance.SendEmail), email);
                i++;
                Console.WriteLine((i + 1) + " emails sent.");
            }

            Console.ReadLine();
        }
    }
}