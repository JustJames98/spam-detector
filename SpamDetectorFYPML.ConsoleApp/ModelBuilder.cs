using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using Microsoft.ML;
using Microsoft.ML.Data;
using SpamDetectorFYPML.Model;

namespace SpamDetectorFYPML.ConsoleApp
{
    public static class ModelBuilder
    {
        private static string TRAIN_DATA_FILEPATH = @"C:\Users\james\Desktop\Third year uni\CS3607 Final Year Project\CSVEmailsEnronSplit.csv";
        private static string MODEL_FILEPATH = @"C:\Users\james\Desktop\Third year uni\CS3607 Final Year Project\Evaluationresults\AllModels\MLModel";
        // Create MLContext to be shared across the model creation workflow objects 
        // Set a random seed for repeatable/deterministic results across multiple trainings.
        private static MLContext mlContext = new MLContext(seed: 1);
        static string trainerUsed = "";
        public static void CreateModel()
        {
            // Load Data
            IDataView trainingDataView = mlContext.Data.LoadFromTextFile<ModelInput>(
                                                path: TRAIN_DATA_FILEPATH,
                                                hasHeader: true,
                                                separatorChar: ',',
                                                allowQuoting: true,
                                                allowSparse: false);
            IEstimator<ITransformer> trainingPipeline;
            ITransformer mlModel = null;
            ModelEvaluation bestModel = new ModelEvaluation
            {
                AverageMicroAccuracy = 0,
                AverageMacroAccuracy = 0,
                ModelData = null,
                ModelSavePath = null,
            };

            for (var i = 0; i < 7; i++)
            {
                // Build training pipeline
                trainingPipeline = BuildTrainingPipeline(mlContext, i);

                Stopwatch stopwatch = new Stopwatch();
                stopwatch.Start();

                // Train Model
                mlModel = TrainModel(mlContext, trainingDataView, trainingPipeline);

                //Evaluate quality of Model
                var evaluationResults = Evaluate(mlContext, trainingDataView, trainingPipeline);

                stopwatch.Stop();
                Console.WriteLine("Elapsed Time is {0} ms", stopwatch.ElapsedMilliseconds);

                var isBetter = CompareModelWithBestModel(bestModel, ReturnMicroAccuracyValues(evaluationResults));

                if (isBetter)
                {
                    bestModel.AverageMicroAccuracy = ReturnMicroAccuracyValues(evaluationResults);
                    bestModel.AverageMacroAccuracy = ReturnMacroAccuracyValues(evaluationResults);
                    bestModel.ModelData = mlModel;
                    bestModel.ModelSavePath = GenerateSavePath(i);
                }
                // Save model
                var modelSaveLocation = GenerateSavePath(i);
                SaveModel(mlContext, mlModel, modelSaveLocation, trainingDataView.Schema);
            }

            Console.WriteLine("The best model is saved in: " + bestModel.ModelSavePath);
            Console.WriteLine("The AverageMicroAccuracy of this model is: " + bestModel.AverageMicroAccuracy);
            Console.WriteLine("The AverageMacroAccuracy of this model is: " + bestModel.AverageMacroAccuracy);
        }

        public static IEstimator<ITransformer> BuildTrainingPipeline(MLContext mlContext, int iterationNumber)
        {
            // Data process configuration with pipeline data transformations 
            var dataProcessPipeline = mlContext.Transforms.Conversion.MapValueToKey("Spam", "Spam")
                                      .Append(mlContext.Transforms.Text.FeaturizeText("EmailSubject_tf", "EmailSubject"))
                                      .Append(mlContext.Transforms.Text.FeaturizeText("EmailBody_tf", "EmailBody"))
                                      .Append(mlContext.Transforms.Concatenate("Features", new[] { "EmailSubject_tf", "EmailBody_tf" }))
                                      .Append(mlContext.Transforms.NormalizeMinMax("Features", "Features"))
                                      .AppendCacheCheckpoint(mlContext);
            // Set the training algorithm 

            IEstimator<ITransformer> trainingPipeline = null;

            if (iterationNumber == 0)
            {
                var trainer = mlContext.MulticlassClassification.Trainers.LightGbm(labelColumnName: @"Spam", numberOfIterations: 100, featureColumnName: "Features")
                    .Append(mlContext.Transforms.Conversion.MapKeyToValue("PredictedLabel", "PredictedLabel"));
                trainerUsed = "LightGbm";
                trainingPipeline = dataProcessPipeline.Append(trainer);
            }
            else if (iterationNumber == 1)
            {
                var trainer = mlContext.MulticlassClassification.Trainers.OneVersusAll(mlContext.BinaryClassification.Trainers.LinearSvm(labelColumnName: "Spam", numberOfIterations: 100, featureColumnName: "Features"), labelColumnName: "Spam")
                                      .Append(mlContext.Transforms.Conversion.MapKeyToValue("PredictedLabel", "PredictedLabel"));
                trainerUsed = "LinearSvm";
                trainingPipeline = dataProcessPipeline.Append(trainer);
            }
            else if (iterationNumber == 2)
            {
                var trainer = mlContext.MulticlassClassification.Trainers.OneVersusAll(mlContext.BinaryClassification.Trainers.SgdCalibrated(labelColumnName: "Spam", numberOfIterations: 100, featureColumnName: "Features"), labelColumnName: "Spam")
                                      .Append(mlContext.Transforms.Conversion.MapKeyToValue("PredictedLabel", "PredictedLabel"));
                trainerUsed = "SgdCalibrated";
                trainingPipeline = dataProcessPipeline.Append(trainer);
            }
            else if (iterationNumber == 3)
            {
                var trainer = mlContext.MulticlassClassification.Trainers.OneVersusAll(mlContext.BinaryClassification.Trainers.SgdNonCalibrated(labelColumnName: "Spam", numberOfIterations: 100, featureColumnName: "Features"), labelColumnName: "Spam")
                                      .Append(mlContext.Transforms.Conversion.MapKeyToValue("PredictedLabel", "PredictedLabel"));
                trainerUsed = "SgdNonCalibrated";
                trainingPipeline = dataProcessPipeline.Append(trainer);
            }
            else if (iterationNumber == 4)
            {
                var trainer = mlContext.MulticlassClassification.Trainers.OneVersusAll(mlContext.BinaryClassification.Trainers.LdSvm(labelColumnName: "Spam", numberOfIterations: 100, featureColumnName: "Features"), labelColumnName: "Spam")
                                      .Append(mlContext.Transforms.Conversion.MapKeyToValue("PredictedLabel", "PredictedLabel"));
                trainerUsed = "LdSvm";
                trainingPipeline = dataProcessPipeline.Append(trainer);
            }
            else if (iterationNumber == 5)
            {
                var trainer = mlContext.MulticlassClassification.Trainers.OneVersusAll(mlContext.BinaryClassification.Trainers.FastForest(labelColumnName: "Spam", numberOfLeaves: 20, featureColumnName: "Features"), labelColumnName: "Spam")
                                      .Append(mlContext.Transforms.Conversion.MapKeyToValue("PredictedLabel", "PredictedLabel"));
                trainerUsed = "FastForest";
                trainingPipeline = dataProcessPipeline.Append(trainer);
            }
            else if (iterationNumber == 6)
            {
                var trainer = mlContext.MulticlassClassification.Trainers.OneVersusAll(mlContext.BinaryClassification.Trainers.FastTree(labelColumnName: "Spam", numberOfLeaves: 20, featureColumnName: "Features"), labelColumnName: "Spam")
                                      .Append(mlContext.Transforms.Conversion.MapKeyToValue("PredictedLabel", "PredictedLabel"));
                trainerUsed = "FastTree";
                trainingPipeline = dataProcessPipeline.Append(trainer);
            }

            return trainingPipeline;
        }

        public static ITransformer TrainModel(MLContext mlContext, IDataView trainingDataView, IEstimator<ITransformer> trainingPipeline)
        {
            Console.WriteLine("=============== Training " + trainerUsed + " model ===============");

            ITransformer model = trainingPipeline.Fit(trainingDataView);

            Console.WriteLine("=============== End of training process ===============");
            return model;
        }

        private static IEnumerable<TrainCatalogBase.CrossValidationResult<MulticlassClassificationMetrics>> Evaluate(MLContext mlContext, IDataView trainingDataView, IEstimator<ITransformer> trainingPipeline)
        {
            // Cross-Validate with single dataset (since we don't have two datasets, one for training and for evaluate)
            // in order to evaluate and get the model's accuracy metrics
            Console.WriteLine("=============== Cross-validating to get model's accuracy metrics ===============");
            var crossValidationResults = mlContext.MulticlassClassification.CrossValidate(trainingDataView, trainingPipeline, numberOfFolds: 10, labelColumnName: "Spam");
            PrintMulticlassClassificationFoldsAverageMetrics(crossValidationResults);

            return crossValidationResults;
        }

        private static void SaveModel(MLContext mlContext, ITransformer mlModel, string modelRelativePath, DataViewSchema modelInputSchema)
        {
            // Save/persist the trained model to a .ZIP file
            Console.WriteLine($"=============== Saving the model  ===============");
            mlContext.Model.Save(mlModel, modelInputSchema, GetAbsolutePath(modelRelativePath));
            Console.WriteLine("The model is saved to {0}", GetAbsolutePath(modelRelativePath));
        }

        public static string GetAbsolutePath(string relativePath)
        {
            FileInfo _dataRoot = new FileInfo(typeof(Program).Assembly.Location);
            string assemblyFolderPath = _dataRoot.Directory.FullName;

            string fullPath = Path.Combine(assemblyFolderPath, relativePath);

            return fullPath;
        }

        public static void PrintMulticlassClassificationFoldsAverageMetrics(IEnumerable<TrainCatalogBase.CrossValidationResult<MulticlassClassificationMetrics>> crossValResults)
        {
            var metricsInMultipleFolds = crossValResults.Select(r => r.Metrics);

            var microAccuracyValues = metricsInMultipleFolds.Select(m => m.MicroAccuracy);
            var microAccuracyAverage = microAccuracyValues.Average();
            var microAccuraciesStdDeviation = CalculateStandardDeviation(microAccuracyValues);
            var microAccuraciesConfidenceInterval95 = CalculateConfidenceInterval95(microAccuracyValues);

            var macroAccuracyValues = metricsInMultipleFolds.Select(m => m.MacroAccuracy);
            var macroAccuracyAverage = macroAccuracyValues.Average();
            var macroAccuraciesStdDeviation = CalculateStandardDeviation(macroAccuracyValues);
            var macroAccuraciesConfidenceInterval95 = CalculateConfidenceInterval95(macroAccuracyValues);

            var logLossValues = metricsInMultipleFolds.Select(m => m.LogLoss);
            var logLossAverage = logLossValues.Average();
            var logLossStdDeviation = CalculateStandardDeviation(logLossValues);
            var logLossConfidenceInterval95 = CalculateConfidenceInterval95(logLossValues);

            var logLossReductionValues = metricsInMultipleFolds.Select(m => m.LogLossReduction);
            var logLossReductionAverage = logLossReductionValues.Average();
            var logLossReductionStdDeviation = CalculateStandardDeviation(logLossReductionValues);
            var logLossReductionConfidenceInterval95 = CalculateConfidenceInterval95(logLossReductionValues);

            Console.WriteLine($"*************************************************************************************************************");
            Console.WriteLine($"*       Metrics for " + trainerUsed + " model      ");
            Console.WriteLine($"*------------------------------------------------------------------------------------------------------------");
            Console.WriteLine($"*       Average MicroAccuracy:    {microAccuracyAverage:0.###}  - Standard deviation: ({microAccuraciesStdDeviation:#.###})  - Confidence Interval 95%: ({microAccuraciesConfidenceInterval95:#.###})");
            Console.WriteLine($"*       Average MacroAccuracy:    {macroAccuracyAverage:0.###}  - Standard deviation: ({macroAccuraciesStdDeviation:#.###})  - Confidence Interval 95%: ({macroAccuraciesConfidenceInterval95:#.###})");
            Console.WriteLine($"*       Average LogLoss:          {logLossAverage:#.###}  - Standard deviation: ({logLossStdDeviation:#.###})  - Confidence Interval 95%: ({logLossConfidenceInterval95:#.###})");
            Console.WriteLine($"*       Average LogLossReduction: {logLossReductionAverage:#.###}  - Standard deviation: ({logLossReductionStdDeviation:#.###})  - Confidence Interval 95%: ({logLossReductionConfidenceInterval95:#.###})");
            Console.WriteLine($"*************************************************************************************************************");

        }

        public static double CalculateStandardDeviation(IEnumerable<double> values)
        {
            double average = values.Average();
            double sumOfSquaresOfDifferences = values.Select(val => (val - average) * (val - average)).Sum();
            double standardDeviation = Math.Sqrt(sumOfSquaresOfDifferences / (values.Count() - 1));
            return standardDeviation;
        }

        public static double CalculateConfidenceInterval95(IEnumerable<double> values)
        {
            double confidenceInterval95 = 1.96 * CalculateStandardDeviation(values) / Math.Sqrt((values.Count() - 1));
            return confidenceInterval95;
        }

        public static double ReturnMicroAccuracyValues(IEnumerable<TrainCatalogBase.CrossValidationResult<MulticlassClassificationMetrics>> crossValResults)
        {
            var metricsInMultipleFolds = crossValResults.Select(r => r.Metrics);

            var microAccuracyValues = metricsInMultipleFolds.Select(m => m.MicroAccuracy);
            var microAccuracyAverage = microAccuracyValues.Average();

            return microAccuracyAverage;
        }

        public static double ReturnMacroAccuracyValues(IEnumerable<TrainCatalogBase.CrossValidationResult<MulticlassClassificationMetrics>> crossValResults)
        {
            var metricsInMultipleFolds = crossValResults.Select(r => r.Metrics);

            var macroAccuracyValues = metricsInMultipleFolds.Select(m => m.MacroAccuracy);
            var macroAccuracyAverage = macroAccuracyValues.Average();

            return macroAccuracyAverage;
        }

        public static bool CompareModelWithBestModel(ModelEvaluation bestModel, double crossValResults)
        {
            if (bestModel.AverageMicroAccuracy < crossValResults)
            {
                return true;
            }
            return false;
        }

        public static string GenerateSavePath(int iterationNumber)
        {
            string trainerUsed = "";
            if (iterationNumber == 0)
            {
                trainerUsed = "LightGbm";
            }
            else if (iterationNumber == 1)
            {
                trainerUsed = "LinearSvm";
            }
            else if (iterationNumber == 2)
            {
                trainerUsed = "SgdCalibrated";
            }
            else if (iterationNumber == 3)
            {
                trainerUsed = "SgdNonCalibrated";
            }
            else if (iterationNumber == 4)
            {
                trainerUsed = "LdSvm";
            }
            else if (iterationNumber == 5)
            {
                trainerUsed = "FastForest";
            }
            else if (iterationNumber == 6)
            {
                trainerUsed = "FastTree";
            }

            var modelSaveLocation = MODEL_FILEPATH + trainerUsed + ".zip";

            return modelSaveLocation;
        }

        public static bool SaveMethods()
        {
            Console.WriteLine("Save every model? Y/N");
            var input = Console.ReadLine();

            switch (input.ToLower())
            {
                case "y":
                    Console.WriteLine("Saving every model");
                    return true;
                default:
                    Console.WriteLine("Saving only the best model");
                    return false;
            }
        }
    }
}
