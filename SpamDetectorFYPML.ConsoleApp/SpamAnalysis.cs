﻿using SpamDetectorFYPML.Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;

namespace SpamDetectorFYPML.ConsoleApp
{
    public class SpamAnalysis
    {
        private const string readPathSpam = "C:\\Users\\james\\Desktop\\Third year uni\\CS3607 Final Year Project\\EnronTestingFiles\\spam";
        private const string readPathHam = "C:\\Users\\james\\Desktop\\Third year uni\\CS3607 Final Year Project\\EnronTestingFiles\\ham";
        public static void RunSpamAnalysis()
        {
            var spamEmails = Directory.EnumerateFiles(readPathSpam, "*.txt");
            var hamEmails = Directory.EnumerateFiles(readPathHam, "*.txt");
            SpamAnalysis spamAnalysisInstance = new SpamAnalysis();
            List<ModelOutput> predictionHam = new List<ModelOutput>();
            List<ModelOutput> predictionSpam = new List<ModelOutput>();

            foreach (var file in spamEmails)
            {
                var emailModel = spamAnalysisInstance.CreateEmailModel(file);
                predictionSpam.Add(ConsumeModel.Predict(emailModel));
            }

            foreach (var file in hamEmails)
            {
                var emailModel = spamAnalysisInstance.CreateEmailModel(file);
                predictionHam.Add(ConsumeModel.Predict(emailModel));
            }

            Console.WriteLine("Positives (Spam correctly marked as Spam): " + predictionSpam.Count(result => result.Prediction == "TRUE"));
            Console.WriteLine("Negatives (Ham correctly marked as Ham): " + predictionHam.Count(result => result.Prediction == "FALSE"));
            Console.WriteLine("False Positives (Ham incorrectly marked as Spam): " + predictionHam.Count(result => result.Prediction == "TRUE"));
            Console.WriteLine("False Negatives (Spam incorrectly marked as Ham): " + predictionSpam.Count(result => result.Prediction == "FALSE"));
        }

        public ModelInput CreateEmailModel(string file)
        {
            var email = ReadEmail(file);
            ModelInput emailModel = new ModelInput
            {
                EmailSubject = FilterEmail.FilterEmailSubject(email),
                EmailBody = FilterEmail.FilterEmailBody(email),
            };

            return emailModel;
        }

        public virtual string ReadEmail(string readPath)
        {
            string result = "";
            try
            {
                using (var sr = new StreamReader(readPath))
                {
                    result = sr.ReadToEnd();
                }
            }
            catch (FileNotFoundException)
            {
                result = "Deleted due to presence of Malicious Objects";
            }
            return result.Replace(",", "");
        }
    }
}
