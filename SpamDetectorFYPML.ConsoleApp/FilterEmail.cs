﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

namespace SpamDetectorFYPML.ConsoleApp
{
    public static class FilterEmail
    {
        public static string FilterEmailSubject(string emailBody)
        {
            Regex subjectRx = new Regex("Subject:.+", RegexOptions.Multiline);

            var subject = subjectRx.Match(emailBody).ToString();

            subject = subject.Replace("\n", "");
            subject = subject.Replace("\r", "");
            subject = subject.Replace(",", "");
            subject = subject.Replace("Subject:", "");
            subject = subject.Trim();

            if (subject.Length == 0)
            {
                subject = "No Subject";
            }

            return RemoveTabsAndMultiSpace(subject);
        }

        public static string FilterEmailBody(string emailBody)
        {
            Regex subjectRx = new Regex("Subject:.+", RegexOptions.Multiline);
            var subject = subjectRx.Match(emailBody).ToString();

            if (string.IsNullOrEmpty(subject))
            {
                subject = "No Subject";
            }

            emailBody = emailBody.Replace(subject, "");
            emailBody = emailBody.Replace("\n", "");
            emailBody = emailBody.Replace("\r", "");
            emailBody = emailBody.Replace(",", "");
            emailBody = emailBody.Trim();

            if (emailBody.Length > 32760)
            {
                emailBody = "Email Size too long";
            }

            if (emailBody.Length == 0)
            {
                emailBody = "No Body";
            }

            return RemoveTabsAndMultiSpace(emailBody);
        }
        public static string RemoveTabsAndMultiSpace(string inputText)
        {
            Regex removeTabsAndMultiSpaceRx = new Regex("\t|\\s+");
            return removeTabsAndMultiSpaceRx.Replace(inputText, " ");
        }
    }
}
